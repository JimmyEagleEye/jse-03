package ru.korkmasov.tsc;


import ru.korkmasov.tsc.constant.TerminalConst;

public class Application {

    /*public final class TerminalConst {
        public static final String CMD_HELP = "help";
        public static final String CMD_VERSION = "version";
        public static final String CMD_ABOUT = "about";
    }*/
    private static void displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("Djalal Korkmasov");
        System.out.println("dkorkmasov@tsc.com");
        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        if (param.equals(TerminalConst.CMD_HELP)) displayHelp();
        if (param.equals(TerminalConst.CMD_VERSION)) displayVersion();
        if (param.equals(TerminalConst.CMD_ABOUT)) displayAbout();
    }

    public static void main(final String[] args) {
        displayWelcome();
        run(args);
    }
}
