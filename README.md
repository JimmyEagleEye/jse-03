# TASK MANAGER

# DEVELOPER INFO

NAME: Djalal Korkmasov

E-MAIL: dkorkmasov@tsconsulting.com

# SOFTWARE

* JDK 15.1

* Windows 10

# HARDWARE

* RAM 16GB

* CPU i7

* HDD 500Gb

# RUN PROGRAM

java -jar ./jse-03.jar

# SCREENSHOTS

